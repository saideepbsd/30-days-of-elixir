
ExUnit.start

defmodule Test do
  
  use ExUnit.Case

  test 'First test' do
    assert 1 == 1.0
  # assert 1 === 1.0
  end

  test 'Refute test' do
    refute 1 === 1.0
  # refute 1 == 1.0 
  end

  test :assert_raise do
    assert_raise ArithmeticError, fn -> :a + 1 end
  # assert_raise RuntimeError, fn -> :a + 1 end
  end

end
